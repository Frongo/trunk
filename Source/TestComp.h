#include "modules/vf_core/vf_core.h"

class TestComp  : public vf::LeakChecked<TestComp>
{
public:
    ~TestComp();

private:
    int thing;
};
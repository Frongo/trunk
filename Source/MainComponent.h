/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef __MAINCOMPONENT_H_1888B6B2__
#define __MAINCOMPONENT_H_1888B6B2__

#include "../JuceLibraryCode/JuceHeader.h"

#include "modules/vf_concurrent/vf_concurrent.h"
#include "TestComp.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainContentComponent   : public Component
{
public:
    //==============================================================================
    MainContentComponent();
    ~MainContentComponent();

    void paint (Graphics&);
    void resized();

private:
    vf::ManualCallQueue queue;

    TestComp* test;
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


#endif  // __MAINCOMPONENT_H_1888B6B2__
